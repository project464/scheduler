run:
	go run ./cmd/


build:
	go build ./cmd/


get:
	curl localhost:3000/reservations

getid:
	curl localhost:3000/reservations/2

post:
	curl -X POST localhost:3000/reservations

delete:
	curl -X DELETE localhost:3000/reservations/123

