package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Collection struct {
	//	Reservations []Reservation `json:"reservations"`
}

type Reservation struct {
	gorm.Model
	Start time.Time `json:"start"`
	End   time.Time `json:"end"`
	Room  int       `json:"room"`
}
