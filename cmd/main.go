package main

import (
	"scheduler/cmd/database"
	"scheduler/cmd/global"
	"scheduler/cmd/reservation"

	"github.com/gofiber/fiber"
)

func helloWorld(c *fiber.Ctx) {
	c.Send([]byte("Hello, World!"))
}

func setupRoutes(app *fiber.App) {
	app.Get("/", helloWorld)

	app.Get("/reservations", reservation.GetReservations)
	app.Get("/reservations/:id", reservation.GetReservation)
	app.Post("/reservations", reservation.NewReservation)
	app.Delete("/reservations/:id", reservation.DeleteReservation)
}

func main() {
	//dbPath := "/home/" + os.Getenv("USER") + "/.ternotes/ternotes.sqlite"
	dbPath := "reservation.db"
	global.DBconn = database.InitDB(dbPath)
	defer global.DBconn.Close()

	//database.MigrateDB(database.DBconn)

	app := fiber.New()

	setupRoutes(app)
	app.Listen(3000)
}
