package database

import (
	"database/sql"
	"fmt"
	"scheduler/cmd/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func InitDB(filepath string) *gorm.DB {
	DBconn, err := gorm.Open("sqlite3", filepath)
	if err != nil || DBconn == nil {
		fmt.Println(err)
		fmt.Println(DBconn)
		panic("Ошибка подключения к базе")
	}
	DBconn.AutoMigrate(&models.Reservation{})
	fmt.Println("Database Migrated")
	return DBconn
}

func MigrateDB(db *sql.DB) {
	sql := `
		CREATE TABLE IF NOT EXISTS notes(
			id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			start DATE DEFAULT NULL,
			end DATE DEFAULT NULL,
			room VARCHAR NOT NULL
		);
	`

	_, err := db.Exec(sql)
	if err != nil {
		panic(err)
	}
}
