package reservation

import (
	"fmt"
	"scheduler/cmd/checks"
	"scheduler/cmd/global"
	"scheduler/cmd/models"
	"time"

	"github.com/gofiber/fiber"
)

func GetReservations(c *fiber.Ctx) {

	reservations := []models.Reservation{}
	global.DBconn.Find(&reservations)
	c.JSON(reservations)
}

func GetReservation(c *fiber.Ctx) {
	id := c.Params("id")
	var reservation models.Reservation
	global.DBconn.Find(&reservation, id)
	c.JSON(reservation)

}

func NewReservation(c *fiber.Ctx) {
	fmt.Println("NewReservation")
	reservation := new(models.Reservation)
	reservation.Start = time.Date(2021, 12, 12, 12, 12, 32, 0, time.UTC)
	reservation.End = time.Date(2021, 12, 12, 13, 12, 32, 0, time.UTC)
	reservation.Room = 5
	fmt.Println(reservation)
	err := global.DBconn.Create(reservation).Error
	checks.CheckErrors(err)
	c.JSON(reservation)
}

func DeleteReservation(c *fiber.Ctx) {
	id := c.Params("id")

	var reservation models.Reservation
	global.DBconn.First(&reservation, id)
	if reservation.Room == 0 {
		c.Status(500).Send("No Reservation Found with ID")
		return
	}
	global.DBconn.Delete(&reservation)
	c.Send(reservation)
}
